/*
Конструкція try...catch дає можливість безперервного виконання коду, навіть, якщо в коді виявляється помилка, а також дозволяє
обробити помилку. Конструкція часто використовується при работі з асінхронними операціями, наприклад запитами на сервер.
Наприклад для декодування даних, отриманих через мережу від сервера або з іншого джерела JavaScript підтримує метод JSON.parse()
 для читання JSON. Якщо дані, які прийшли від стороннього джерела мають синтаксичну помилку формату JSON треба використовувати
 конструкцію try...catch для уникнення генерації помилки при використанні JSON.parse методу, тому що генерація помилки спричинить
 зупинку JavaScript.
 Інший приклад - робота із файлами. У серверному оточенні JavaScript має доступ до файлової системи. При записі може статися помилка. Однак після
  відкриття файл дуже важливо закрити. У таких випадках використовується finally, щоб обов'язково закрити файл.
  На даний момент для завантаження даних стає все популярнішим використання async/await функцій. Однак і в них можуть бути помилки.
  Тому можна використовувати блоки try ... catch для обробки помилок в асинхронних функціях.
 */

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const div = document.getElementById("root");
const ul = document.createElement("ul");
div.append(ul);

function checking (book){
   if(!book.author){
       throw new Error("The object does not have the author property");
   }else if(!book.name) {
       throw new Error("The object does not have the name property");
   }else if(!book.price) {
       throw new Error("The object does not have the price property");
   }
}

 function addElements (){
     books.forEach(book => {
         try {
             checking(book);
             const li = document.createElement("li");
             ul.append(li);
             li.textContent = `${book.author}, ${book.name}, ${book.price}`;
         } catch (err) {
             console.log(err);
         }
     });
 }
addElements ();


